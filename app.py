from flask import Flask, render_template, Response
import datetime
from io import BytesIO
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.dates import DayLocator, DateFormatter

app = Flask(__name__)

@app.route("/")
def hello_world():
    return render_template('index.html')

@app.route("/graph")
def graph():
    str_date = datetime.datetime.now()
    return render_template('graph.html', date=str_date)

@app.route('/plot.png')
def plot_png():
    fig = create_figure()
    output = BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')

def create_figure():

    # dataframe
    df = pd.read_excel("./data/Weight.xlsx", sheet_name='Data')

    df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')
    mask = (df['date'] < pd.to_datetime('today'))
    df = df.loc[mask]

    print(mask)

    # Create the Figure    
    fig = Figure()
    ax = fig.subplots()
    ax.plot(df['date'], df['kilos'])

    #defines the label format
    ax.xaxis.set_major_formatter(DateFormatter("%m-%Y"))
    ax.tick_params(axis="x", labelrotation= 90)

    return fig

@app.route('/last', methods=['GET'])
def append_timestamp():
    test_param = request.args.get('98bd1c45684cf587ac2347a92dd7bb51')
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")  # Generate a timestamp

    with open('/home/health/last.log', 'a') as file:
        file.write(f"{timestamp}: {test_param}\n")

    with open('/home/health/last.log', 'r') as file:
        lines = file.readlines()[-14:]

    return "<br>".join(lines)
